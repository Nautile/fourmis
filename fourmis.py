import random
import math
import matplotlib.pyplot as plt
import numpy as np
import xlrd
import networkx as nx

u=0
v=0

donnees = xlrd.open_workbook("VOIES_NM_440009.xlsx")
sheet = donnees.sheet_by_index(0)
num = sheet.col_values(6, 1)
libelle = sheet.col_values(1, 1)
numTenant = sheet.col_values(8, 1)
numAboutissant = sheet.col_values(10, 1)
lon = sheet.col_values(13, 1)

rues = zip(num,libelle, numTenant, numAboutissant, lon)


graph=nx.Graph()
for num,libelle, numTenant,numAboutissant,lon in rues:
	if(numTenant!=0):
		graph.add_edge(int(num), int(numTenant), {'rue': libelle, 'weigth':abs(lon), 'pheromone' : 0})
	if(numAboutissant !=0):
		graph.add_edge(int(num), int(numAboutissant), {'rue': libelle, 'weigth':abs(lon), 'pheromone' : 0})

source = 30
target = 210
shortPath = nx.shortest_path(graph,source,target)
edge_labels=dict([((u,v,),d['rue'])
for u,v,d in graph.edges(data=True)])
pos=nx.spring_layout(graph)
nx.draw_networkx_edge_labels(graph,pos,edge_labels)
nx.draw(graph,pos)
plt.show()

nbAnt = 2
nbcycle=50
nb_street= len(list(rues))
c=1
nodeInitial=30
nodeFinal=210
e = nodeInitial
find_path=[]
path_length=[]
path=[]
badPath= []
selection = []
y=[]
f = []
ant = {'initial_pheromone':1,'alpha':1,'beta':2,'pheromone_deposit':1,'evaporation_constant':0.8}

#calculer la distance inverse
def calcVisibility(node, neighbor):
	w=getWeightEdge(node,neighbor)
	attraction = 1/w
	return attraction

#calculer  l'intensité
def calcIntensity(node,neighbor, ant):
        calc = (math.pow(getPheromoneEdge(node,neighbor), ant['alpha']))*(math.pow(calcVisibility(node,neighbor),ant['beta']))
        return calc

#récupérer le node suivant
def getNextNode(node,path,nodeInitial,nodeFinal,badPath,ant):
	neighbors = graph.neighbors(node)
	newNeighbors = deleteBadPath(node,neighbors,path,badPath,nodeFinal)
	if(len(newNeighbors)==0):
		nextNode = returnStateBefore(node,path,badPath)
	else:
		havePheromone = havePheromoneEdge(node,newNeighbors)
		nextNode = chooseBestNextNode(node,newNeighbors,havePheromone,ant)
	return nextNode

#verifier si l'on peut recuperer un pheromone
def havePheromoneEdge(e,newNeighbors):
	b =  False
	for t in newNeighbors:
		if(getPheromoneEdge(e,t)>0.0):
			b=True
	return b

#Calculer l'intensité
def calcIntensity2(newNeighbors,node,ant):
	intensity = 0
	for neighbor in newNeighbors:
		intensity = intensity + calcIntensity(node,neighbor, ant)
	return intensity

#choisir le meilleur node
def chooseBestNextNode(e,newNeighbors,havePheromone,ant):
	weigth=[]
	choice = []
	neighborsL=len(newNeighbors)
	if(neighborsL>1):
		if(havePheromone==True):
			for r in newNeighbors:
				firstcalc = calcIntensity(e,r, ant)
				secondcalc = calcIntensity2(newNeighbors,e,ant)
				
				choice.append(firstcalc/secondcalc)
			randome = random.random()
			if(randome >0.6):
				n = newNeighbors[choice.index(max(choice))]
			else:
				n = newNeighbors[random.randrange(0, neighborsL-1, 1)]
		else:
			n = newNeighbors[random.randrange(0, neighborsL-1, 1)]
	else:
		n = newNeighbors[0]
	return n

#suprimmer le mauvais chemin
def deleteBadPath(e,neighbors,path,badPath,nodeFinal):
	listToDelete = []
	for a in neighbors:
		futurNeighbors = graph.neighbors(a)
		if(a in badPath or a in path):
			listToDelete.append(a)
	for y in listToDelete:
		neighbors.remove(y)
	return neighbors

#Evaporer un pheromone
def evaporate(path,ant):
	for i, j in zip(path, path[1:]):
		pheromoneEdge = graph[i][j]['pheromone']
		pheromone = pheromoneEdge*(1-ant['evaporation_constant'])
		graph[i][j]['pheromone']=pheromone

#Construction du chemin
def definePath(i,path):
	path.append(i)
	if(len(path)>1 and path[-1]==path[-2]):
		path.remove(path[-1])
	return path

#Retourner en arrière
def returnStateBefore(e,path,badPath):
	if path[-1] not in badPath:
		badPath.append(path[-1])
	path.remove(path[-1])
	f=path[-1]
	return f

#Mettre à jour une fourmis
def updatePheromone(path,ant):
	for i, j in zip(path, path[1:]):
		pheromoneEdge = graph[i][j]['pheromone']
		weigth = graph[i][j]['weigth']
		pheromone = pheromoneEdge + (ant['pheromone_deposit']/weigth)
		graph[i][j]['pheromone']=pheromone

#Recuperer le poids des pheromones
def getPheromoneEdge(i,nextNode):
	pheromoneEdge = graph[i][nextNode]['pheromone']
	return pheromoneEdge

#Récupérer le poids d'un edge
def getWeightEdge(i,nextNode):
	weight = graph[i][nextNode]['weigth']
	return weight

#Récupérer le nom d'un Edge
def getNameEdge(i,nextNode):
	name = graph[i][nextNode]['rue'].encode('utf-8')
	return name

#Calculer la longueur du chemin d'une solution
def calcPathLength(path):
	w = 0
	for i, j in zip(path, path[1:]):
		weightEdge = graph[i][j]['weigth']
		w = w + weightEdge
	return w

#Selectionner le meilleur chemin
def selectBestPath(path):
	path_length=[]
	for f in path:
		path_length.append(calcPathLength(f))
	y.append(path_length[path_length.index(min(path_length))])
	return path[path_length.index(min(path_length))]

f.append(c)
while c < nbcycle:
	j=0
	while j < nbAnt:
		path=[]
		find_path = []
		e=nodeInitial
		while (e != nodeFinal):
			path = definePath(e,path)
			nextNode = getNextNode(e,path,nodeInitial,nodeFinal,badPath,ant)
			if(nextNode==nodeFinal):
				path = definePath(nextNode,path)
				e=nextNode
			else:
				e=nextNode		
		find_path.append(path)
		updatePheromone(path,ant)
		evaporate(path,ant)
		j = j +1
	selection.append(selectBestPath(find_path))
	c = c + 1
	f.append(c)
MyshortestPath = selectBestPath(selection)

print ("\n------------------------------------------------------------\n")
print ("Chemin le plus court (networkx) : " + str(shortPath))
print ('Chemin le plus court (fourmis) ' + str(MyshortestPath))

print ("\n------------------------------------------------------------\n")
for q, s in zip(MyshortestPath, MyshortestPath[1:]):
	print ('Rue '+ str(MyshortestPath.index(s))+' : ' + str(getNameEdge(q,s)))

donnees = xlrd.open_workbook("VOIES_NM_440009.xlsx")
sheet = donnees.sheet_by_index(0)
num = sheet.col_values(6, 1)
libelle = sheet.col_values(1, 1)
numTenant = sheet.col_values(8, 1)
numAboutissant = sheet.col_values(10, 1)
lon = sheet.col_values(13, 1)

rues = zip(num,libelle, numTenant, numAboutissant, lon)
secondGraph=nx.Graph()
for num,libelle, numTenant,numAboutissant,lon in rues:
	if(num in MyshortestPath):
		if(numTenant!=0 and numTenant in MyshortestPath):
			secondGraph.add_edge(int(num), int(numTenant), {'rue': libelle, 'weigth':abs(lon)})
		if(numAboutissant !=0 and numAboutissant in MyshortestPath):
			secondGraph.add_edge(int(num), int(numAboutissant), {'rue': libelle, 'weigth':abs(lon)})

edge_labels=dict([((u,v,),d['rue'])
for u,v,d in secondGraph.edges(data=True)])
pos=nx.spring_layout(secondGraph)
nx.draw_networkx_edge_labels(secondGraph,pos,edge_labels)
nx.draw(secondGraph,pos)
plt.show()
